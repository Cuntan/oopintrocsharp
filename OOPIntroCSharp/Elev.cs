﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPIntroCSharp
{
    class Elev : Persoana,IHasExport
    {
        private int nota;
        private string scoala;
        public Elev() : base("Default Elev", 2010)
        {
            nota = 5;
            scoala = "Inginerie Sibiu";
        }

        public Elev(string name, int yearOfBirth, int nota, string scoala) : base(name, yearOfBirth)
        {
            this.nota = nota;
            this.scoala = scoala;
        }

        public override void ConsoleWrite()
        {
            Console.WriteLine(name + " - " + yearOfBirth + "  - " + scoala + "  nota:" + nota);
        }

        public new void Export()
        {
            Console.WriteLine("Am exportat elevul:" + name);
        }
    }
}
